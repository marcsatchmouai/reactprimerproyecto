// const element = document.createElement('h1')
// element.innerText = 'Hello React..'
// const container = document.getElementById('root')
// container.appendChild(element)

import React from 'react'
import ReactDOM from 'react-dom'
import Card from './components/Card'
import 'bootstrap/dist/css/bootstrap.css'


// const user = {
//     firstName: 'Raul',
//     lastName: 'Palacios',
//     avatar: 'https://cdn4.iconfinder.com/data/icons/avatars-xmas-giveaway/128/batman_hero_avatar_comics-256.png'
// }

// function getName(user){
//     return `${user.firstName} ${user.lastName}`
// }

// function getGreeting(user){
//     if(user){
//         return <h1>Hello {getName(user)}</h1>
//     }
//     return <h1>Hello Stranger</h1>
// }
// const name = 'Raul'
// //const element = <div>{getGreeting(user)}</div>
// //const element = <img src={user.avatar}/>
// const element = (
//     <div>
//         {getGreeting(user)}
//         <img src={user.avatar}/>
//     </div>
// )
//ReactDOM.render(element, container)

const container = document.getElementById('root')
ReactDOM.render(<Card 
                    title="Technique Guides"
                    description="Learn amazing street workout and calisthenics"
                    img="https://firebasestorage.googleapis.com/v0/b/tutoriales-e4830.appspot.com/o/exercise.png"
                    leftColor="#A74CF2"
                    rightColor="#617BFB"
                />, container)